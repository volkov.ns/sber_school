package com.nv.consultation.hw1.task6;

import com.nv.consultation.hw1.task1.MyCheckedException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.GregorianCalendar;

public class FormValidator {
    /**
     * Длина имени должна быть от 2 до 20 символов, первая буква заглавная.
     */
    public static void checkName(String str) {
//        if (str == null) throw new FormValidatorException("my message");
//        // считаем что 2 включительно
//        if (str.length() < 2) {
//            throw new FormValidatorException("Минимальная длина для имени 2 символа");
//        }
//        // считаем что 20 не включительно
//        else if (str.length() > 20) {
//            throw new FormValidatorException("Максимальная длина для имени 20 символов");
//        }
//
//        if (!Character.isUpperCase(str.charAt(0))) {
//            throw new FormValidatorException("Имя должно начинаться с заглавной буквы");
//        }

        if (str == null
                || str.length() < 2
                || str.length() > 20
                || !Character.isUpperCase(str.charAt(0))) {
            throw new FormValidatorException("Длина имени должна быть от 2 до 20 символов, первая буква заглавная.");
        }
    }

    /**
     * Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.
     */
    public static void checkBirthdate(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate inputDate;

        try {
            inputDate = LocalDate.parse(str, formatter);
        } catch (DateTimeParseException e) {
            throw new FormValidatorException(
                    "Дата рождения должна иметь формат дд.мм.гггг"
            );
        }

        LocalDate lowestDate = LocalDate.parse("01.01.1900", formatter);
        LocalDate todayDate  = LocalDate.now();

        // Защита от 29.02.2022 в невискосный год
        // LocalDate::parse понижает дату до 28.02.2022
        if (!formatter.format(inputDate).equals(str)) {
            throw new FormValidatorException(String.format(
                    "Ошибка при конвертировании даты, возможно введена несуществующая дата, введено %s, дата после конвертации %s",
                    str,
                    formatter.format(inputDate)
            ));
        }

        if (inputDate.isBefore(lowestDate)) {
            throw new FormValidatorException("Дата рождения должна быть не раньше 01.01.1900");
        }

        if (inputDate.isAfter(todayDate)) {
            throw new FormValidatorException("Дата рождения должна быть не позже " + formatter.format(todayDate));
        }
    }

    /**
     * Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.
     */
    public static void checkBirthdateV2(String str) {
        // Можно заменить "SimpleDateFormat" и ".matches" на DateTimeFormatter.ofPattern("dd.MM.yyyy")?
        // DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        if (!str.matches("\\d{2}\\.\\d{2}\\.\\d{4,}")) {
            throw new FormValidatorException(
                    "Дата рождения должна иметь формат дд.мм.гггг"
            );
        }

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        GregorianCalendar formValue = new GregorianCalendar();

        try {
            dateFormat.parse(str);
            formValue.setTimeInMillis(dateFormat.getCalendar().getTimeInMillis());
        } catch (ParseException parseException) {
            throw new FormValidatorException(
                    "Дата рождения должна иметь формат дд.мм.гггг"
            );
        }

        // Защита от 29.02.2022 в невискосный год
        // SimpleDateFormat::parse воспринимает как 1 марта
        if (!dateFormat.format(formValue.getTime()).equals(str)) {
            throw new FormValidatorException(
                    "Ошибка при конвертировании даты, возможно введена несуществующая дата"
            );
        }

        GregorianCalendar minDate = new GregorianCalendar(
                1900, GregorianCalendar.JANUARY, 1
        );

        if (formValue.compareTo(minDate) < 0) {
            throw new FormValidatorException(
                    "Дата рождения должна быть не раньше 01.01.1900"
            );
        }

        GregorianCalendar maxDate = new GregorianCalendar();
        maxDate.set(GregorianCalendar.HOUR, 0);
        maxDate.set(GregorianCalendar.MINUTE, 0);
        maxDate.set(GregorianCalendar.SECOND, 0);
        maxDate.set(GregorianCalendar.MILLISECOND, 0);

        if (formValue.compareTo(maxDate) > 0) {
            throw new FormValidatorException(
                    "Дата рождения должна быть не позже " + dateFormat.format(maxDate.getTime())
            );
        }
    }

    /**
     * Пол должен корректно матчится в enum Gender, хранящий Male и Female значения.
     */
    public static void checkGender(String str) {
        try {
            Gender.valueOf(str);
        } catch (IllegalArgumentException exception) {
            throw new FormValidatorException(String.format(
                    "Для поля \"Gender\" передано невалидное значение \"%s\", допустимые значения: %s",
                    str,
                    Arrays.toString(Gender.values())
            ));
        }
    }

    /**
     * Рост должен быть положительным числом и корректно конвертироваться в double.
     */
    public static void checkHeight(String str) {
        double height;

        try {
            height = Double.parseDouble(str);
        } catch (NumberFormatException exception) {
            throw new FormValidatorException(
                    "Рост должен быть числом, в формате \"метры.сантиметры\""
            );
        }

        // Считаем что 0 не положительное и не отрицательное
        if (height <= 0) {
            throw new FormValidatorException("Рост должен быть положительным числом");
        }
    }

    public enum Gender {
        MALE("male"), FEMALE("female");

        private String value;

        Gender(String value) {
            this.value = value;
        }

        public static Gender getByString(String s) {
            if (s == null) throw new FormValidatorException("");
            for (Gender g : Gender.values()) {
                if (g.value.toUpperCase().equals(s.toUpperCase())) {
                    return g;
                }
            }
            throw new FormValidatorException("");
        }
    }
}
