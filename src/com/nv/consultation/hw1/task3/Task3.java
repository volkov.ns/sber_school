package com.nv.consultation.hw1.task3;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл ./output.txt
 * текст из input, где каждый латинский строчный символ заменен на соответствующий заглавный.
 * Обязательно использование try с ресурсами.
 */
public class Task3 {

    private static final char LETTER_A_CODE = 96;

    public static void main(String[] args) {
        readAndWrite();
    }

    private static void readAndWrite() {
        try (
                FileReader reader = new FileReader("./PM/Section1/Part1/Task3/input.txt");
                FileWriter writer = new FileWriter("./PM/Section1/Part1/Task3/output.txt")
        ) {
            int decimalCode;

            while ((decimalCode = reader.read()) != -1) {
                // Каждый латинский строчный символ заменяем на соответствующий заглавный
                if (decimalCode > LETTER_A_CODE && decimalCode < 123) {
                    writer.write(Character.toUpperCase(decimalCode));
                } else {
                    writer.write(decimalCode);
                }
            }
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
