package com.nv.exceptions.practice;

public class MyBaseException extends RuntimeException {
    public MyBaseException(String message) {
        super(message);
        System.out.println(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
