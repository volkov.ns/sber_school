package com.nv.consultation.hw4.task1;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

//Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
//экран.
public class Main {
//    public static void main(String[] args) {
//        System.out.println(IntStream.rangeClosed(1, 100)
//                .filter(i -> i % 2 == 0)
//                .reduce(0, (a, b) -> a += b));
//    }

    public static void main(String[] args) {
        System.out.println(
                IntStream.rangeClosed(1, 100)
                        .filter(i -> i % 2 == 0)
                        .sum());
    }
}
