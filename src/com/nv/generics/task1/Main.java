package com.nv.generics.task1;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Pair<String, Number> pair1 = new Pair<>();
        pair1.setFirst("s");
        pair1.setSecond(5);
        System.out.println(pair1);
        Pair<String, Long> pair2 = new Pair<>();
        pair2.setFirst("s");
        pair2.setSecond(10L);
        System.out.println(pair2);
    }
}
