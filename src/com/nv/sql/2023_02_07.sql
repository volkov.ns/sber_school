create table public.books (
    id serial primary key,
    title varchar(30) not null,
    author varchar(30) not null,
    date_added timestamp not null
);

commit;

delete from public.books
where title is null;

select *
from information_schema.columns
where table_name = 'books';

drop table public.books;

select *
from public.books;

select current_timestamp ;

insert into public.books(title, author, date_added)
values ('Недоросль', 'Д. И. Фонвизин', now());
insert into public.books(author, date_added)
values ('Д. И. Фонвизин', now() - interval '25h');

insert into public.books(id, title, author, date_added)
values (1, 'Недоросль', 'Д. И. Фонвизин', now());

alter TABLE public.books
alter COLUMN title TYPE varchar(100);

alter TABLE public.books
alter COLUMN title drop NOT NULL;

insert into public.books(title, author, date_added)
values (
    'Путешествие из Петербурга в Москву',
     'А. Н. Радищев',
      now() - interval '24h');
insert into public.books(title, author, date_added)
values (
    'Доктор Живаго',
    'Б. Л. Пастернак',
     now() - interval '24h');
insert into public.books(title, author, date_added)
values (
    'Сестра моя - жизнь',
    'Б. Л. Пастернак',
    now());

--Удалить элемент с id = 1
delete from public.books
where id = 1;

--Удалить все  элементы из таблицы
delete from public.books;
truncate table public.books; --сбрасывает ключи
--Удалить таблицу
drop table public.books;

commit;


create table public.books (id serial primary key,  title       varchar(100) not null,  author     varchar(100) not null, date_added timestamp   not null);
insert into public.books(title, author, date_added) values ('Недоросль', 'Д. И. Фонвизин', now());
insert into public.books(title, author, date_added) values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
insert into public.books(title, author, date_added) values ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
insert into public.books(title, author, date_added) values ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());

select row_number() over () rownum, offset, t.*
from public.books t;

select b.*
from public.books as b
limit 2 offset 2;

--Достать запись под id = 2
select *
from public.books
where ID=2;

select *
from public.books
order by date_added desc, author desc;

--Найти автора книги по названию ‘Недоросль’
select author
from public.books
where title = 'Недоросль';
--Найти все книги Пастернака
select *
from public.books
where author like '%Пастернак%';

--Вывести максимальный id в таблице
select max(id)
from public.books;

--Найти все книги Радищева или Пастернака
--отсортированные по дате в обратном порядке
select *
from public.books
where author like '%Радищев%'
    or author like '%Пастернак%'
order by date_added desc;
--Найти все книги Пастернака добавленные вчера
select *
from public.books
where author like '%Пастернак%'
--    and date_added <= now() - interval '24h'
--    and date_added >= now() - interval '48h'
    and date_added between now() - interval '48h'
                        and now() - interval '24h';
commit;
--Обновить время добавления для книги с id = 2
update public.books
set date_added = now()
where id = 2;

select (regexp_split_to_array(author, E'\\. '))[3], b.*
from public.books b;

-- Оставить только фамилии авторами книг
UPDATE public.books
SET author = (regexp_split_to_array(author, E'\\. '))[3];

commit;

CREATE TABLE IF NOT EXISTS public.reviews
(
   id       bigserial primary key,
   book_id  integer REFERENCES public.books,
   reviewer varchar(100) NOT NULL,
   rating   integer NOT NULL,
   comment  text NULL
);

INSERT INTO public.reviews(book_id, reviewer, rating, comment) VALUES (1, 'Петя', 10, 'отличная книга');
INSERT INTO public.reviews(book_id, reviewer, rating) VALUES (1,'Кирилл', 9);
INSERT INTO public.reviews(book_id, reviewer, rating, comment) VALUES (3, 'Петя', 7, 'ок');
INSERT INTO public.reviews(book_id, reviewer, rating, comment) VALUES (4, 'Иннокентий', 2, 'не понравилась');

commit;

select * from public.reviews;

--Достать только те записи reviews,
--у которых comment != null
select *
from public.reviews
where comment IS NOT NULL;

--Посчитать сколько всего записей в reviews.
--Назвать столбец Количество отзывов
select count(*) as "Количество отзывов"
from public.reviews;

--Узнать количество уникальных id книг,
--по которым были оставлены отзывы:
select count(DISTINCT book_id)
from public.reviews;

--Вывести сколько review по каждой id книги
select count(*), book_id
from public.reviews
group by book_id
having count(*) > 2;

--Вывести все значения по books и по reviews
--(объединение столбцов результатов)
select b.*, '|',  r.*
from public.books b, public.reviews r
where b.id = r.book_id;


--ИЛИ через Join
select b.*, '|',  r.*
from public.books b
full join public.reviews r
    ON b.id = r.book_id and r.comment is not null;


--А через left join добъемся вывода книги, для которой нет отзывов
select * from books b
left join reviews r
ON b.id = r.book_id



--Вычислить среднюю оценку по каждой книге.
--Вывести столбцы Оценка, Название книги.
SELECT sum(rating::float) / COUNT(*) as "Средняя оценка",
 b.title as "Название книги"
FROM public.reviews r
  INNER JOIN public.books b ON r.book_id = b.id
GROUP BY b.title;

--Удалить книгу с id = 1 из books
delete from public.books where id = 3;
select * from public.reviews;
rollback;
--не получится, т.к. есть fk
--Можно либо сначала удалить все review c book_id = 1
delete from public.reviews where book_id = 1;
commit;

--Либо при создании fk указывать ON DELETE CASCADE. Изменить существующий fk нельзя, можно удалить и создать новый:
ALTER TABLE public.reviews
drop CONSTRAINT reviews_book_id_fkey;

ALTER TABLE public.reviews
ADD CONSTRAINT reviews_book_id_fkey
    FOREIGN KEY (book_id)
    REFERENCES public.books (id)
    ON DELETE CASCADE ON UPDATE NO ACTION;


--Посмотреть какие есть pk, fk в таблицах:
SELECT conrelid::regclass AS table_from
     , conname
     , pg_get_constraintdef(oid),
     p.*
FROM   pg_constraint p
WHERE  contype IN ('f', 'p ')
AND    connamespace = 'public'::regnamespace
ORDER  BY conrelid::regclass::text, contype DESC;