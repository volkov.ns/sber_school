package com.nv.exceptions.practice;

import java.io.FileNotFoundException;

public class Example {
    public static void main(String[] args) {
//        try {
//            throwRun();
//            throwIllegal();
//        } catch (IllegalArgumentException | FileNotFoundException e) {
//           throw new MyArithmeticException();
//        }
//
//        throwIllegal();
        qwe();
    }

    private static void qwe() {
        try {
            throwIllegal();
        } catch (IllegalArgumentException e) {
            throw new MyArithmeticException("qwe", e);
        }

    }

    private static void throwRun() throws FileNotFoundException {
        throw new FileNotFoundException("file");
    }

    private static void throwIllegal() {
        throw new IllegalArgumentException("illegal");
    }
}
