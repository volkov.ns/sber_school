package com.nv.stream.task1;

import com.nv.inner.task2.IntSquare;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

//Использовать реализованный функциональный интерфейс Square на массиве чисел, вывести на экран:
public class Main {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3 ,4);
        list.stream()
                .map(i -> i*i)
                .forEach(System.out::println);
//        list.forEach(i -> System.out.println(i*i));
    }
}
