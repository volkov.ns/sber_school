package com.nv.consultation.hw2.task5;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("the","day","is","sunny",
                "sunny","is","is","day");
        int k = 2;
        Map<String, Integer> map = new HashMap<>();

        for (String word : words) {
            Integer count = map.get(word);
            if (count == null) {
                count = 1;
            } else {
                ++count;
            }
            map.put(word, count);
        }
        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(map.entrySet());
        System.out.println(entryList);
        entryList.sort((e1, e2) -> e2.getValue().compareTo(e1.getValue()));
        List<String> result = entryList.subList(0, k)
                .stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        System.out.println(result);
    }
}
