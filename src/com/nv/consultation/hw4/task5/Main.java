package com.nv.consultation.hw4.task5;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//На вход подается список непустых строк. Необходимо привести все символы строк к
//        верхнему регистру и вывести их, разделяя запятой.
//        Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
public class Main {
//    public static void main(String[] args) {
//        StringBuilder sb = new StringBuilder();
//        Stream.of("abc", "def", "qqq")
//                .map(String::toUpperCase)
//                .forEach(s -> sb.append(s).append(", "));
//        sb.delete(sb.length() - 2, sb.length());
//        System.out.println(sb.append("."));
//    }

    public static void main(String[] args) {

        List<String> list = List.of("abc", "def", "qqq");

        System.out.println(
                list.stream()
                        .map(String::toUpperCase)
                        .collect(Collectors.joining(", ")));

    }
}
