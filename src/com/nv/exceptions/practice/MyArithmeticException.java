package com.nv.exceptions.practice;

public class MyArithmeticException extends RuntimeException {

    public MyArithmeticException() {
        super("Do not divide by 0");
    }

    public MyArithmeticException(String message) {
        super(message);
    }

    public MyArithmeticException(String message, Throwable cause) {
        super(message, cause, false, true);
    }
}
