package com.nv.consultation.hw4.task3;

import java.util.List;

//На вход подается список строк. Необходимо вывести количество непустых строк в
//        списке.
//        Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
public class Main {
    public static void main(String[] args) {
        System.out.println(List.of("abc", "", "", "def", "qqq").stream()
                .filter(s -> {
                    if (s == null || s.isEmpty()) return false;
                    return true;})
                .count());
    }
}
