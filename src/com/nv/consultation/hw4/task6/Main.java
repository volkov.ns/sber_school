package com.nv.consultation.hw4.task6;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

//Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> setOfSets = Set.of(Set.of(4, 2, 3), Set.of(0, 1, 5), Set.of(3, 5, -2));
        System.out.println(setOfSets.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet()));
    }
}
