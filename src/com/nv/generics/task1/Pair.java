package com.nv.generics.task1;

//любого типа
//public class Pair <T extends String, V extends Number> {
//    public T first;
//    public V second;
//}

import java.util.Objects;

//    одинакового типа
//public class Pair <T> {
//    public T first;
//    public T second;
//}
//
//
//    первый только String, второй только числовой
public class Pair <T extends String, V extends Number> {
    private T first;
    private V second;

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public V getSecond() {
        return second;
    }

    public void setSecond(V second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(first, pair.first) && Objects.equals(second, pair.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}


