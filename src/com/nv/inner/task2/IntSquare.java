package com.nv.inner.task2;

@FunctionalInterface
public interface IntSquare {
    int calculate(int x);
}
