package com.nv.generics.task5;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    //    На вход подается строка, состоящая из маленьких латинских символов.
//    Проверить, что в строке встречаются все символы английского алфавита хотя бы раз:
//    thequickbrownfoxjumpsoverthelazydog -> true
//    sdfaaaa -> false
    public static void main(String[] args) {
        System.out.println(checkString("thequickbrownfoxjumpsoverthelazydog"));
        System.out.println(checkString("qwertyuiopasdkkfkkgjkakjdiweeoidjaoqj"));
        System.out.println(checkString("sdfaaaa"));
        System.out.println(checkString(null));
    }

    public static boolean checkString(String sentence) {
        if (sentence == null || sentence.length() < 26) return false;
        Set<Character> letters = new HashSet<>();
        for (char c : sentence.toCharArray()) {
            letters.add(c);
        }
        if (letters.size() == 26) return true;
        return false;
    }
}
