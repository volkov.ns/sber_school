package com.nv.stream.task4;

//Проверить, является ли текст палиндром.
// Из исходной строки с помощью стримов убрать все символы,
// не являющиеся цифрой или буквой, привести к нижнему регистру.
public class Main {
    public static void main(String[] args) {
        String s = "qWERtY88288;';'';'yTrewQ";
        StringBuilder firstString = new StringBuilder();
        s.chars()
                .filter(Character::isLetterOrDigit)
                .map(Character::toLowerCase)
                .forEach(firstString::appendCodePoint);
        StringBuilder secondString = new StringBuilder(firstString).reverse();
        System.out.println(secondString.toString().equals(firstString.toString()));
    }
}
