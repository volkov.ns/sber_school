package com.nv.inner.task1;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Timer {
    private long timeNanoseconds = 0;

    public void measureTime(Runnable runnable) {
        long startTime = System.nanoTime();
        runnable.run();
        timeNanoseconds += System.nanoTime() - startTime;
    }

    public long getTimeNanoseconds() {
        return timeNanoseconds;
    }
}
