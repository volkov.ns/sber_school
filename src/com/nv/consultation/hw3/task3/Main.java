package com.nv.consultation.hw3.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        try {
            Class<APrinter> aPrinterClass = APrinter.class;
            Method method = aPrinterClass.getMethod("print", int.class);
            method.invoke(new APrinter(), 192929);
        } catch (IllegalAccessException e) {
            System.out.println("The method access modifiers forbid calling it");
        } catch (InvocationTargetException e) {
            System.out.println("The method has thrown an exception");
        } catch (NoSuchMethodException e) {
            System.out.println("Cannot find method by name");
        }
    }
}
