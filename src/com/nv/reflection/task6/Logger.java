package com.nv.reflection.task6;

public enum Logger {
    INFO,
    DEBUG,
    ERROR
}
