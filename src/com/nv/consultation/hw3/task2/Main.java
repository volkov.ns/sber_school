package com.nv.consultation.hw3.task2;

import com.nv.consultation.hw3.task1.IsLike;


public class Main {
    public static void main(String[] args) {
        SimpleClass simpleClass = new SimpleClass();
        checkIsLike(simpleClass);
    }

    private static void checkIsLike(Object o) {
        Class<?> clazz = o.getClass();
        IsLike annotation = clazz.getAnnotation(IsLike.class);
        System.out.println(annotation.value());
    }
}
