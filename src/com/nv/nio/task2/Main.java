package com.nv.nio.task2;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//Создать папку и подпаку в src/
public class Main {
    public static void main(String[] args) {
        Path path1 = Paths.get("src/myFolder");
        Path path2 = Paths.get("src/myFolder/myAnotherFolder");
        try {
            Files.createDirectory(path1);
            Files.createDirectory(path2);
        } catch (FileAlreadyExistsException e) {
            System.out.println("File already exists: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
