package com.nv.stream.task2;

import java.math.BigInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.List;

//Посчитать n! с помощью стримов
public class Main {
    public static void main(String[] args) {
        long startTime = System.nanoTime();

        System.out.println(IntStream.rangeClosed(1, 10000000)
                .mapToObj(BigInteger::valueOf)
                .reduce(BigInteger.ZERO, BigInteger::add));

        System.out.println(System.nanoTime() - startTime);

        startTime = System.nanoTime();
        List<BigInteger> list = IntStream.rangeClosed(1, 10000000)
                .mapToObj(BigInteger::valueOf)
                .collect(Collectors.toList());
        System.out.println(list.parallelStream()
                .reduce(BigInteger.ZERO, BigInteger::add, BigInteger::add));

        System.out.println(System.nanoTime() - startTime);
    }
}
// stream - 3.9
// parallel - 0.05