package com.nv.nio.task1;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

//Создать файл в src/ с названием test.txt
public class Main {
    public static void main(String[] args) {
        Path path = Paths.get("src/test.txt");
        try {
            System.out.println(Files.createFile(path));
        } catch (FileAlreadyExistsException e) {
            System.out.println("File already exists: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
