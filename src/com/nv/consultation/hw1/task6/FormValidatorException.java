package com.nv.consultation.hw1.task6;

public class FormValidatorException extends IllegalArgumentException {
    public FormValidatorException(String s) {
        super(s);
    }
}
