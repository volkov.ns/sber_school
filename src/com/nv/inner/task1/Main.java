package com.nv.inner.task1;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

//Создать таймер, который считает время выполнения метода, используя Runnable.
public class Main {
    public static void main(String[] args) {
        Timer timer = new Timer();
//        timer.measureTime(new Runnable() {
//            @Override
//            public void run() {
//                long sum = 0;
//                for (int i = 1; i <= 1_000_000_000; ++i) {
//                    sum += i;
//                }
//                System.out.println(sum);
//            }
//        });
        Runnable runnable = () -> {
            BigInteger sum = BigInteger.ZERO;
            for (int i = 1; i <= 100000000; ++i) {
                sum = sum.add(BigInteger.valueOf(i));
            }
            System.out.println(sum);
        };


//        timer.measureTime(() -> {
//            long sum = 0;
//            for (int i = 1; i <= 1_000_000_000; ++i) {
//                sum += i;
//            }
//            System.out.println(sum);
//        });

        timer.measureTime(runnable);
        System.out.println(timer.getTimeNanoseconds());
    }
}
//364099800
//360868300
//717776100
