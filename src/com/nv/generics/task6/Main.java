package com.nv.generics.task6;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);

        System.out.println(set);

        set.removeIf(elem -> elem % 2 != 0);

//        for (int i = 0; i < set.size(); i++) {
//            Integer elem = set.get(i);
//            if (elem % 2 != 0) {
//                set.remove(elem);
//            }
//        }

//        for (Integer elem : set) {
//            if (elem % 2 != 0) {
//                set.remove(elem);
//            }
//        }

        System.out.println(set);
    }
}
