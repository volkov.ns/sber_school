package com.nv.inner.task2;

//С помощью функционального интерфейса выполнить подсчет квадрата числа:
public class Main {
    public static void main(String[] args) {
        IntSquare intSquare = x -> x*x;
        System.out.println(intSquare.calculate(5));
    }
}
