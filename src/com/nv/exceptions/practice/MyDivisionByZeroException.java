package com.nv.exceptions.practice;

public class MyDivisionByZeroException extends MyBaseException {
    public MyDivisionByZeroException(String message) {
        super(message);
    }

    public MyDivisionByZeroException() {
        super("Can't divide by zero");
    }
}
