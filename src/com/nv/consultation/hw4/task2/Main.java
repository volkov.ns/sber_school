package com.nv.consultation.hw4.task2;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.IntStream;

//На вход подается список целых чисел. Необходимо вывести результат перемножения
//        этих чисел.
//        Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
//        120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
public class Main {
//    public static void main(String[] args) {
////        System.out.println(IntStream.rangeClosed(1, 5)
////                .mapToObj(BigInteger::valueOf)
////                .reduce(BigInteger.ONE, BigInteger::multiply));
//
//        System.out.println(IntStream.rangeClosed(1, 5)
//                .reduce(1, (a, b) -> a *= b));
//    }

    public static void main(String[] args) {

        List<Integer> list = List.of(1, 2, 3, 4, 5); //или другие целые числа


        //1 вариант (для работы с большими числами и/или длинным списком,
        // то есть ожидаемый результат может привести к переполнению Integer)

        System.out.println(list.parallelStream()
                .map(BigInteger::valueOf)
                .reduce(BigInteger.ONE, BigInteger::multiply));


        //2 вариант (если известно, что ожидаемый результат не приведет к переполнению Integer
        // (работа с маленькими числами и/или с небольшим количесвом чисел)

        System.out.println(list.stream().reduce(1, (a, b) -> a * b));

    }
}
