package com.nv.consultation.hw4.task4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

//На вход подается список вещественных чисел. Необходимо отсортировать их по
//        убыванию.
public class Main {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(DoubleStream.of(1.3, 3.4, 953.89, 745.8347)
                .boxed()
//                .sorted((d1, d2) -> {
//                    if (d2 > d1) {
//                        return 1;
//                    } else if (d2 < d1) {
//                        return -1;
//                    }
//                    return 0;})
                        .sorted((d1, d2) -> d2.compareTo(d1))
                .toArray())
        );
    }

//    public class Task_4 {
//        public static void main(String[] args) {
//
//            List<Double> list = List.of(2.2, -111.11, 555.5, -3.3, 0.0);
//
//            List<Double> listSorted =
//                    list.stream()
//                            .sorted(Collections.reverseOrder())
//                            .collect(Collectors.toList());
//
//            System.out.println(listSorted);
//        }
//    }
}
