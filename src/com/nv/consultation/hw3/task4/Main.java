package com.nv.consultation.hw3.task4;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(getAllInterfaces(ArrayList.class));//реализация на примере класса ArrayList<E>.
    }

    public static List<Class<?>> getAllInterfaces(Class<?> clazz) {
        List<Class<?>> result = new ArrayList<>();
        Class<?> currentClass = clazz;
        while (currentClass != null) {
            List<Class<?>> interfaces = Arrays.asList(currentClass.getInterfaces());
            result.addAll(interfaces);
            for (Class<?> i : interfaces) {
                result.addAll(getAllInterfaces(i));
            }
            currentClass = currentClass.getSuperclass();
        }
        return result;
    }
}