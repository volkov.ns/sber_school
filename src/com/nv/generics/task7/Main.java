package com.nv.generics.task7;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<Double, String> hm1 = new HashMap<>();

        hm1.put(1d, "one");
        hm1.put(2d, "two");
        hm1.put(3d, "three");

        System.out.println(hm1);

        Iterator<Map.Entry<Double, String>> entryIterator = hm1.entrySet().iterator();

        for (Map.Entry<Double, String> entry : hm1.entrySet()) {
            entry.setValue("qwe");
        }

//        System.out.println(hm1);

        System.out.println("Элементы мапы: " + hm1);

        System.out.println("Значение элемента 2 " + hm1.get(2.0));
    }
}
