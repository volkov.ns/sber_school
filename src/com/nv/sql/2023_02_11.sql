commit;

rollback;

--Создать таблицу книг со следующими полями
--id ключ
--title- наименование книги
--author - автор книги
--date_added - дата добавления книги

create table public.books (
    id  serial primary key,
    title varchar(30) not null,
    author varchar(30) not null,
    date_added timestamp not null
);

select *
from public.books;

insert into public.books(title, author, date_added)
values ('Недоросль', 'Д. И. Фонвизин', now());
insert into public.books(title, author, date_added)
values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
insert into public.books(title, author, date_added)
values ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
insert into public.books(title, author, date_added)
values ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());

alter table public.books
alter column title type varchar(100);

delete from public.books
where title = 'Недоросль';

delete from public.books
where title like '%а%';

truncate table public.books;

drop table public.books;

create table public.books (id serial primary key,  title       varchar(100) not null,  author     varchar(100) not null, date_added timestamp   not null);
insert into public.books(title, author, date_added) values ('Недоросль', 'Д. И. Фонвизин', now());
insert into public.books(title, author, date_added) values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
insert into public.books(title, author, date_added) values ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
insert into public.books(title, author, date_added) values ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());

select * from public.books;

--Достать запись под id = 2
select *
from public.books
where id = 2;
--Найти автора книги по названию ‘Недоросль’
select *
from public.books
where title = 'Недоросль';
--Найти все книги Пастернака
select * from public.books
where author like '%Пастернак'
order by id;
--Вывести максимальный id в таблице
select max(id) from public.books;
--Найти все книги Радищева или Пастернака отсортированные по дате в обратном порядке
select *
from public.books
where author like '%Пастернак%' or author like '%Радищев%'
order by date_added desc;
--Найти все книги Пастернака добавленные вчера
select *
from public.books
where author like '%Пастернак%'
    and date_added <= now() - interval '24h'
    and date_added > now() - interval '48h';

update public.books
set date_added = '2020-02-02'
where id = 2;

update public.books
set author = (regexp_split_to_array(author, E'\\.'))[3];

select (regexp_split_to_array(author, E'\\.'))[3], *
from public.books;

select * from public.books;

 create table if not exists public.reviews
(
   id       bigserial primary key,
   book_id  integer references public.books,
   reviewer varchar(100) not null,
   rating   integer not null,
   comment  text null
);

select * from public.reviews;

commit;

insert into public.reviews(book_id, reviewer, rating, comment)
values (1, 'Петя', 10, 'отличная книга');
insert into public.reviews(book_id, reviewer, rating)
values (1,'Кирилл', 9);
insert into public.reviews(book_id, reviewer, rating, comment)
values (3, 'Петя', 7, 'ок');
insert into public.reviews(book_id, reviewer, rating, comment)
values (4, 'Иннокентий', 2, 'не понравилась');

--Достать только те записи reviews, у которых comment != null
select *
from public.reviews
where comment is not null;
--Посчитать сколько всего записей в reviews. Назвать столбец Количество отзывов
select
    count(*) as "Количество отзывов"
from public.reviews;
--Узнать количество уникальных id книг, по которым были оставлены отзывы:
select count(distinct(book_id))
from public.reviews
where comment is not null;
--Вывести сколько review по каждой id книги
select book_id, count(*)
from public.reviews
group by book_id;
--Вывести все значения по books и по reviews (объединение столбцов результатов)
select *
from public.reviews r, public.books b
where r.book_id = b.id;

select *
from public.reviews r
    full join public.books b on r.book_id = b.id;

--Вычислить среднюю оценку по каждой книге. Вывести столбцы Оценка, Название книги.
select b.title, sum(r.rating::float) / count(*), avg(r.rating)
from public.reviews r
    inner join public.books b on r.book_id = b.id
group by b.title;
--Удалить книгу с id = 1 из books
delete from public.reviews where book_id = 1;
delete from public.books where id = 1;

select * from public.reviews;
select * from public.books;

alter table public.reviews
drop constraint reviews_book_id_fkey;

alter table public.reviews
add constraint reviews_book_id_fkey
    foreign key (book_id)
    references public.books(id)
    on delete cascade on update no action;

select * from public.reviews;
select * from public.books;

delete from public.books where id = 3;